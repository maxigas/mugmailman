#!/bin/bash
if [ $# -ne 1 ]; then echo 'Usage: mugmailman.sh $URL'; exit 2; else URL=$1; fi
files=$(wget -O - $URL | egrep -o '".*?\.txt\.gz"' | sed 's/\"//g')
for f in $files; do wget $URL$f; done
echo ================================
echo DONE: Saved mailing list archive to the current directory from "$URL"
gzip -d *
tar cfz archive.tar.gz *txt
echo ================================
echo DONE: Created single file archive archive.tar.gz

